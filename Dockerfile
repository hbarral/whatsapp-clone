# build stage
FROM node:14.21.3-alpine3.16 as build-stage

WORKDIR /whatsapp-clone
COPY package*.json /whatsapp-clone/
RUN apk update

RUN node --max-old-space-size=300 `which npm` install

COPY . /whatsapp-clone/

RUN node --max-old-space-size=300 `which npm` run build

# production stage
FROM nginx:1.17.9-alpine as production-stage
COPY --from=build-stage /whatsapp-clone/build /usr/share/nginx/html
COPY --from=build-stage /whatsapp-clone/build /home/admin/web/whatsapp-clone/public_html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

